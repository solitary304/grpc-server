package vn.com.application.grpc;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.15.0)",
    comments = "Source: user-proto.proto")
public final class CustomerServiceGrpc {

  private CustomerServiceGrpc() {}

  public static final String SERVICE_NAME = "vn.com.application.CustomerService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<vn.com.application.grpc.CarCenter.CustomerRequest,
      vn.com.application.grpc.CarCenter.Message> getCreateRequestMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "createRequest",
      requestType = vn.com.application.grpc.CarCenter.CustomerRequest.class,
      responseType = vn.com.application.grpc.CarCenter.Message.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<vn.com.application.grpc.CarCenter.CustomerRequest,
      vn.com.application.grpc.CarCenter.Message> getCreateRequestMethod() {
    io.grpc.MethodDescriptor<vn.com.application.grpc.CarCenter.CustomerRequest, vn.com.application.grpc.CarCenter.Message> getCreateRequestMethod;
    if ((getCreateRequestMethod = CustomerServiceGrpc.getCreateRequestMethod) == null) {
      synchronized (CustomerServiceGrpc.class) {
        if ((getCreateRequestMethod = CustomerServiceGrpc.getCreateRequestMethod) == null) {
          CustomerServiceGrpc.getCreateRequestMethod = getCreateRequestMethod = 
              io.grpc.MethodDescriptor.<vn.com.application.grpc.CarCenter.CustomerRequest, vn.com.application.grpc.CarCenter.Message>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "vn.com.application.CustomerService", "createRequest"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  vn.com.application.grpc.CarCenter.CustomerRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  vn.com.application.grpc.CarCenter.Message.getDefaultInstance()))
                  .setSchemaDescriptor(new CustomerServiceMethodDescriptorSupplier("createRequest"))
                  .build();
          }
        }
     }
     return getCreateRequestMethod;
  }

  private static volatile io.grpc.MethodDescriptor<vn.com.application.grpc.CarCenter.DelegateRequest,
      vn.com.application.grpc.CarCenter.Message> getDelegateRequestMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "delegateRequest",
      requestType = vn.com.application.grpc.CarCenter.DelegateRequest.class,
      responseType = vn.com.application.grpc.CarCenter.Message.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<vn.com.application.grpc.CarCenter.DelegateRequest,
      vn.com.application.grpc.CarCenter.Message> getDelegateRequestMethod() {
    io.grpc.MethodDescriptor<vn.com.application.grpc.CarCenter.DelegateRequest, vn.com.application.grpc.CarCenter.Message> getDelegateRequestMethod;
    if ((getDelegateRequestMethod = CustomerServiceGrpc.getDelegateRequestMethod) == null) {
      synchronized (CustomerServiceGrpc.class) {
        if ((getDelegateRequestMethod = CustomerServiceGrpc.getDelegateRequestMethod) == null) {
          CustomerServiceGrpc.getDelegateRequestMethod = getDelegateRequestMethod = 
              io.grpc.MethodDescriptor.<vn.com.application.grpc.CarCenter.DelegateRequest, vn.com.application.grpc.CarCenter.Message>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "vn.com.application.CustomerService", "delegateRequest"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  vn.com.application.grpc.CarCenter.DelegateRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  vn.com.application.grpc.CarCenter.Message.getDefaultInstance()))
                  .setSchemaDescriptor(new CustomerServiceMethodDescriptorSupplier("delegateRequest"))
                  .build();
          }
        }
     }
     return getDelegateRequestMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static CustomerServiceStub newStub(io.grpc.Channel channel) {
    return new CustomerServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static CustomerServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new CustomerServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static CustomerServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new CustomerServiceFutureStub(channel);
  }

  /**
   */
  public static abstract class CustomerServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void createRequest(vn.com.application.grpc.CarCenter.CustomerRequest request,
        io.grpc.stub.StreamObserver<vn.com.application.grpc.CarCenter.Message> responseObserver) {
      asyncUnimplementedUnaryCall(getCreateRequestMethod(), responseObserver);
    }

    /**
     */
    public void delegateRequest(vn.com.application.grpc.CarCenter.DelegateRequest request,
        io.grpc.stub.StreamObserver<vn.com.application.grpc.CarCenter.Message> responseObserver) {
      asyncUnimplementedUnaryCall(getDelegateRequestMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getCreateRequestMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                vn.com.application.grpc.CarCenter.CustomerRequest,
                vn.com.application.grpc.CarCenter.Message>(
                  this, METHODID_CREATE_REQUEST)))
          .addMethod(
            getDelegateRequestMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                vn.com.application.grpc.CarCenter.DelegateRequest,
                vn.com.application.grpc.CarCenter.Message>(
                  this, METHODID_DELEGATE_REQUEST)))
          .build();
    }
  }

  /**
   */
  public static final class CustomerServiceStub extends io.grpc.stub.AbstractStub<CustomerServiceStub> {
    private CustomerServiceStub(io.grpc.Channel channel) {
      super(channel);
    }

    private CustomerServiceStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected CustomerServiceStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new CustomerServiceStub(channel, callOptions);
    }

    /**
     */
    public void createRequest(vn.com.application.grpc.CarCenter.CustomerRequest request,
        io.grpc.stub.StreamObserver<vn.com.application.grpc.CarCenter.Message> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getCreateRequestMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void delegateRequest(vn.com.application.grpc.CarCenter.DelegateRequest request,
        io.grpc.stub.StreamObserver<vn.com.application.grpc.CarCenter.Message> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getDelegateRequestMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class CustomerServiceBlockingStub extends io.grpc.stub.AbstractStub<CustomerServiceBlockingStub> {
    private CustomerServiceBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private CustomerServiceBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected CustomerServiceBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new CustomerServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public vn.com.application.grpc.CarCenter.Message createRequest(vn.com.application.grpc.CarCenter.CustomerRequest request) {
      return blockingUnaryCall(
          getChannel(), getCreateRequestMethod(), getCallOptions(), request);
    }

    /**
     */
    public vn.com.application.grpc.CarCenter.Message delegateRequest(vn.com.application.grpc.CarCenter.DelegateRequest request) {
      return blockingUnaryCall(
          getChannel(), getDelegateRequestMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class CustomerServiceFutureStub extends io.grpc.stub.AbstractStub<CustomerServiceFutureStub> {
    private CustomerServiceFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private CustomerServiceFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected CustomerServiceFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new CustomerServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<vn.com.application.grpc.CarCenter.Message> createRequest(
        vn.com.application.grpc.CarCenter.CustomerRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getCreateRequestMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<vn.com.application.grpc.CarCenter.Message> delegateRequest(
        vn.com.application.grpc.CarCenter.DelegateRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getDelegateRequestMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_CREATE_REQUEST = 0;
  private static final int METHODID_DELEGATE_REQUEST = 1;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final CustomerServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(CustomerServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_CREATE_REQUEST:
          serviceImpl.createRequest((vn.com.application.grpc.CarCenter.CustomerRequest) request,
              (io.grpc.stub.StreamObserver<vn.com.application.grpc.CarCenter.Message>) responseObserver);
          break;
        case METHODID_DELEGATE_REQUEST:
          serviceImpl.delegateRequest((vn.com.application.grpc.CarCenter.DelegateRequest) request,
              (io.grpc.stub.StreamObserver<vn.com.application.grpc.CarCenter.Message>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class CustomerServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    CustomerServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return vn.com.application.grpc.CarCenter.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("CustomerService");
    }
  }

  private static final class CustomerServiceFileDescriptorSupplier
      extends CustomerServiceBaseDescriptorSupplier {
    CustomerServiceFileDescriptorSupplier() {}
  }

  private static final class CustomerServiceMethodDescriptorSupplier
      extends CustomerServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    CustomerServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (CustomerServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new CustomerServiceFileDescriptorSupplier())
              .addMethod(getCreateRequestMethod())
              .addMethod(getDelegateRequestMethod())
              .build();
        }
      }
    }
    return result;
  }
}
