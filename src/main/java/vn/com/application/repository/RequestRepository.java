package vn.com.application.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.com.application.entity.Request;

@Repository
public interface RequestRepository extends JpaRepository<Request, Long> {

}