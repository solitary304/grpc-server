package vn.com.application.config;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import vn.com.application.service.CustomerService;

@Component
public class GrpcServerConfig implements CommandLineRunner {

    @Autowired
    public CustomerService customerService;

    @Override
    public void run(String... args) throws Exception {
        Server server = ServerBuilder.forPort(1234).addService(customerService).build();
        server.start();
        System.out.println(String.format("server started with port %s", server.getPort()));
        server.awaitTermination();
    }
}
