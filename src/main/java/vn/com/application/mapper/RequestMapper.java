package vn.com.application.mapper;

import vn.com.application.entity.Request;
import vn.com.application.grpc.CarCenter;

public class RequestMapper {

    public static Request requestToEntity(CarCenter.CustomerRequest customerRequest){
        Request request = new Request();
        request.setRequestTypeId(customerRequest.getRequestTypeValue());
        request.setName(customerRequest.getName());
        request.setModel(customerRequest.getCarModel());
        return request;
    }

    public static CarCenter.CustomerRequest entityToRequest(Request entity){
        return CarCenter.CustomerRequest.newBuilder()
                .setRequestType(CarCenter.RequestType.forNumber(entity.getRequestTypeId()))
                .setName(entity.getName())
                .setCarModel(entity.getModel())
                .build();
    }
}
