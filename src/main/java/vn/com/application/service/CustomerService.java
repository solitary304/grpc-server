package vn.com.application.service;


import io.grpc.stub.StreamObserver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import vn.com.application.grpc.CarCenter;
import vn.com.application.grpc.CustomerServiceGrpc;
import vn.com.application.mapper.RequestMapper;
import vn.com.application.repository.RequestRepository;

@Component
public class CustomerService extends CustomerServiceGrpc.CustomerServiceImplBase {

    @Autowired
    private RequestRepository requestRepository;

    @Override
    public void createRequest(CarCenter.CustomerRequest request, StreamObserver<CarCenter.Message> responseObserver) {
        CarCenter.RequestType requestType = request.getRequestType();
        String customerName = request.getName();
        String model = request.getCarModel();

        CarCenter.Message.Builder builder = CarCenter.Message.newBuilder();
        switch (requestType) {
            case BUY:
                builder.setMessage(String.format("Đại gia %s muốn mua %s chạy Grab.", customerName, model))
                        .setRequestId(requestType.getNumber()).build();
                break;
            case FIX:
                builder.setMessage(String.format("Thằng %s muốn sửa chiếc %s.", customerName, model))
                        .setRequestId(requestType.getNumber()).build();
                break;
            case SELL:
                builder.setMessage(String.format("Ronaldo mới đá bay chiếc %s của thằng %s. Nó muốn bán xe trả nợ.", model, customerName))
                        .setRequestId(requestType.getNumber()).build();
                break;
            default:
                builder.setMessage("request này lỗi nha.....")
                        .setRequestId(requestType.getNumber()).build();
                break;
        }
        requestRepository.save(RequestMapper.requestToEntity(request));
        responseObserver.onNext(builder.build());
        responseObserver.onCompleted();
    }

    @Override
    public void delegateRequest(CarCenter.DelegateRequest request, StreamObserver<CarCenter.Message> responseObserver) {

    }
}
