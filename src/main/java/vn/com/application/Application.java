package vn.com.application;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import vn.com.application.grpc.CustomerServiceGrpc;
import vn.com.application.service.CustomerService;

import java.io.IOException;

@SpringBootApplication
public class Application {

    public static void main(String[] args) throws InterruptedException, IOException {
        SpringApplication.run(Application.class, args);
    }
}
